package com.redmilitante.dao;

import com.redmilitante.domain.Forum;

public interface ForumDAO {

	public void insertForum(Forum forum);
	public Forum selectForum(int forumId);
	
}
